#include <iostream>

//przykład deklaracji stałej
#define DIV 6.9

int main()
{
    //int - liczba całkowita 64 bit
    //float - liczba zmiennoprzecinkowa pojedynczej precyzji
    //double - liczba zmiennoprzecinkowa podwójnej precyzji
    //bool - prawda/fałsz fałsz = 0; >0 - prawda
    //char - pojedynczy znak 8bit
    //char[] <=> char* - talica znakowa
    // unsigned char/int/float/double - liczba bez znaku
    //long [long] int/float/double - poszerzone zakresy zmiennych
    /************************************************
    \t - tabluacja
    \n - nowa linia (new line)
    \r - powrót kursor/karetka (return carret)
    \a - dzwonek (bell)
    \b - cofnięcie (backspace)
    \\ - powoduje wyświetlenie backslash
    \" - wydrukowanie znakdu cudzysłowia
    \' - wydrukowanie znaku apostrofu
    http://en.cppreference.com/w/cpp/language/escape
    *************************************************/
    //int a = 0;
    //int b = 0;
    //int c = 0;
    //równoważny zapis poniżej
    // int a=0,b=0;
    int a=0,
        b=0;
    //zmienne zmiennoprzecinkowe powinny być domyślnie
    //inicjowane wartością z literą f na końcu (upewnienie, że
    //kompilator będzie je traktował jako zmiennoprzecinkowe)
    float d = .0f;
//    std::cout << "Podaj pierwsza liczbe: ";
//    std::cin >> a;
//    std::cout << "\nPodaj druga liczbe: ";
//    std::cin >> b;
    //poniższa 2 linie z powodzeniem zastępują 4 linie powyżej
    std::cout << "Podaj dwie liczby oraz imie: ";
    std::cin >> a >> b;

    d = a + b;
    std::cout << "\nSuma " << a << " oraz " << b <<
                 " wynosi: " << d << '\n';
    return 0;
}

